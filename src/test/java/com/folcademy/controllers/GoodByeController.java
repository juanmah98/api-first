package com.folcademy.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GoodByeController {
    @RequestMapping("/goodbye")
    public String goodbye(){
        return "Goodbye World!";
    }
}
